package unitqueue.daos;


import unitqueue.pojos.TblBalanceSnaps;

import java.util.List;

/**
 * Created by michaelwheeler on 2017/02/28.
 *
 */
public interface BalanceSnapsDao {
    void saveBalanceSnap(TblBalanceSnaps balanceSnap);
    int saveBalanceSnaps(List<TblBalanceSnaps> balanceSnaps);

}
