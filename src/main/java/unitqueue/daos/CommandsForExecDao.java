package unitqueue.daos;


import unitqueue.pojos.ViewCommandsForExec;

import java.util.List;

/**
 * Created by michaelwheeler on 2017/02/28.
 */
public interface CommandsForExecDao {
    List<ViewCommandsForExec> getCommandsToExecute();
    void updateCommandAsExecuted(ViewCommandsForExec toBeUpdated);
}