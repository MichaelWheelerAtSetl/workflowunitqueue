package unitqueue.configuration;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import unitqueue.daos.BalanceSnapsDao;
import unitqueue.daos.CommandsForExecDao;
import unitqueue.impl.BalanceSnapsDaoImpl;
import unitqueue.impl.CommandsForExecDaoImpl;
import unitqueue.pojos.TblBalanceSnaps;
import unitqueue.pojos.ViewCommandsForExec;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by michaelwheeler on 2017/01/17.
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "main.unitqueue.impl,main.unitqueue.configuration")
public class HibernateAdminConfiguration {
    @Autowired
    public  SessionFactory sessionFactory;

    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) {
        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(
                dataSource);
        sessionBuilder.addProperties(getHibernateProperties());
        sessionBuilder.addAnnotatedClasses(ViewCommandsForExec.class);
        sessionBuilder.addAnnotatedClasses(TblBalanceSnaps.class);

        return sessionBuilder.buildSessionFactory();
    }

    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/setlnet?noAccessToProcedureBodies=true");
        dataSource.setUsername("root");
        dataSource.setPassword("AlbertRo55");
        return dataSource;
    }


    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.dialect",
                "org.hibernate.dialect.MySQLDialect");

        return properties;
    }

    @Bean(name = "transactionManager")
    public HibernateTransactionManager getTransactionManager(
            SessionFactory sessionFactory) {
        return new HibernateTransactionManager(
                sessionFactory);
    }

    @Bean (name = "commandsForExecDao")
    public CommandsForExecDao getCommandsForExecDao() {
        return new CommandsForExecDaoImpl();
    }

    @Bean
    public HibernateTransactionManager transactionManager() {
        HibernateTransactionManager transactionManager =
                new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory);
        return transactionManager;
    }
    @Bean (name = "balanceSnapsDao")
    public BalanceSnapsDao getBalanceSnapsDao() {
        return new BalanceSnapsDaoImpl();
    }

}

