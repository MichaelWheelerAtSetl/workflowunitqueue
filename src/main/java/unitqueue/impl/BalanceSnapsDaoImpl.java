package unitqueue.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import unitqueue.daos.BalanceSnapsDao;
import unitqueue.pojos.TblBalanceSnaps;

import java.util.List;

/**
 * Created by michaelwheeler on 2017/02/28.
 */
@Repository
public class BalanceSnapsDaoImpl implements BalanceSnapsDao {
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    @Transactional
    public void saveBalanceSnap(TblBalanceSnaps balanceSnap) {
        sessionFactory.getCurrentSession().saveOrUpdate(balanceSnap);
    }

    @Override
    @Transactional
    public int saveBalanceSnaps(List<TblBalanceSnaps> balanceSnaps) {
        int offset =0;
        for (TblBalanceSnaps snap: balanceSnaps) {
            sessionFactory.getCurrentSession().save(snap);
            if (offset++ == 1000) {
                sessionFactory.getCurrentSession().flush();
                sessionFactory.getCurrentSession().clear();
            }
        }
        return balanceSnaps.size();
    }

}
