package unitqueue.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import unitqueue.daos.CommandsForExecDao;
import unitqueue.pojos.CommandIdPK;
import unitqueue.pojos.ViewCommandsForExec;

import java.util.List;

/**
 * Created by michaelwheeler on 2017/02/28.
 */

/**
 * Update entities in unit queue as 'In Progress'
 * Update entities in unit queue as 'Completed'
 */

@Repository
public class CommandsForExecDaoImpl implements CommandsForExecDao {
    private final String SQL_PROCEDURE_EXECUTED = "update_dependency_as_processed";
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public List<ViewCommandsForExec> getCommandsToExecute() {
        List<ViewCommandsForExec> listUser = (List<ViewCommandsForExec>) sessionFactory.getCurrentSession().createQuery("from ViewCommandsForExec").list();
        return listUser;
    }

    @Transactional
    private void updateUnitQueueCommand(String procedureName, CommandIdPK commandId) {
        String sqlQuery = String.format("CALL %s (%s, %s, %d, %d)", procedureName, commandId.getAccountId().toString(), commandId.getUnitId().toString(), commandId.getStep(), commandId.getSubStep());
        Query q =  sessionFactory.getCurrentSession().createSQLQuery(sqlQuery);
        int result = q.executeUpdate();
        System.out.print(result);
    }

    @Override
    public void updateCommandAsExecuted(ViewCommandsForExec toBeUpdated) {
        updateUnitQueueCommand(SQL_PROCEDURE_EXECUTED, toBeUpdated.getCommandId());
    }
}
