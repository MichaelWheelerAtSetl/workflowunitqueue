package unitqueue.pojos;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by michaelwheeler on 2017/02/28.
 */
@Entity
@Table(name = "view_CommandsForExec", schema = "setlnet")
public class ViewCommandsForExec {

    private Integer dependency;
    private Timestamp executionTime;
    private String command;
    private String params;
    private Integer rollbackStep;
    private Timestamp created;
    private Byte recoveryStep;
    private Byte processing;

    @EmbeddedId
    private CommandIdPK commandId;

    public CommandIdPK getCommandId() {
        return this.commandId;
    }

    @Basic
    @Column(name = "dependency", nullable = true)
    public Integer getDependency() {
        return dependency;
    }

    public void setDependency(Integer dependency) {
        this.dependency = dependency;
    }

    @Basic
    @Column(name = "executionTime", nullable = false)
    public Timestamp getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Timestamp executionTime) {
        this.executionTime = executionTime;
    }

    @Basic
    @Column(name = "command", nullable = false, length = -1)
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Basic
    @Column(name = "params", nullable = true)
    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Basic
    @Column(name = "rollbackStep", nullable = true)
    public Integer getRollbackStep() {
        return rollbackStep;
    }

    public void setRollbackStep(Integer rollbackStep) {
        this.rollbackStep = rollbackStep;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "recoveryStep", nullable = false)
    public Byte getRecoveryStep() {
        return recoveryStep;
    }

    public void setRecoveryStep(Byte recoveryStep) {
        this.recoveryStep = recoveryStep;
    }

    @Basic
    @Column(name = "processing", nullable = false)
    public Byte getProcessing() { return processing;}

    public void setProcessing(Byte processing) {this.processing = processing;}
}
