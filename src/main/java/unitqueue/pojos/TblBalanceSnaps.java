package unitqueue.pojos;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by michaelwheeler on 2017/02/28.
 */
@Entity
//@IdClass(TblBalanceSnapsPK.class)
@Table(name = "tblBalanceSnaps", schema = "setlnet")
public class TblBalanceSnaps implements Serializable{
    private Long account;
    private String name;
    private Long amount;
    private Long accountId;
    private Long unitId;

    @Id
    @Column(name = "account", nullable = false)
    public Long getAccount() {
        return account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "amount", nullable = false, precision = 2)
    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    @Id
    @Column(name = "accountID", nullable = false)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Id
    @Column(name = "unitID", nullable = false)
    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        TblBalanceSnaps that = (TblBalanceSnaps) o;
//
//        if (account != null ? !account.equals(that.account) : that.account != null) return false;
//        if (name != null ? !name.equals(that.name) : that.name != null) return false;
//        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
//        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) return false;
//        if (unitId != null ? !unitId.equals(that.unitId) : that.unitId != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = account != null ? account.hashCode() : 0;
//        result = 31 * result + (name != null ? name.hashCode() : 0);
//        result = 31 * result + (amount != null ? amount.hashCode() : 0);
//        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
//        result = 31 * result + (unitId != null ? unitId.hashCode() : 0);
//        return result;
//    }
}
