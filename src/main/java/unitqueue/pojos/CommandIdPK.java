package unitqueue.pojos;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by michaelwheeler on 2017/03/03.
 */

@Embeddable
public class CommandIdPK implements Serializable{
    private Long accountId;
    private Long unitId;
    private Integer step;
    private Integer subStep;

    @Basic
    @Column(name = "accountID", nullable = false)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }
    @Basic
    @Column(name = "unitID", nullable = false)
    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }
    @Basic
    @Column(name = "step", nullable = false)
    public Integer getStep() {
        return step;
    }
    public void setStep(Integer step) {
        this.step = step;
    }

    @Basic
    @Column(name = "subStep", nullable = false)
    public Integer getSubStep() {
        return subStep;
    }

    public void setSubStep(Integer subStep) {
        this.subStep = subStep;
    }

}