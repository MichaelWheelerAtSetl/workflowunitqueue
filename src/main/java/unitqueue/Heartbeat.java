package unitqueue;

/**
 * Created by michaelwheeler on 2017/02/28.
 */
public class Heartbeat implements Runnable {

    @Override
    public void run() {
        while (true) {
            System.out.println("Chugging...");
            try {
                Thread.sleep(2500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
