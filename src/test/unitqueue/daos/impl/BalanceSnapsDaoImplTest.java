package unitqueue.daos.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import unitqueue.configuration.HibernateContextConfiguration;
import unitqueue.daos.BalanceSnapsDao;
import unitqueue.pojos.TblBalanceSnaps;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by michaelwheeler on 2017/02/28.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateContextConfiguration.class})
public class BalanceSnapsDaoImplTest {


    @Autowired
    private BalanceSnapsDao balanceSnapsDao;

    @Test
    public void saveBalanceSnap() {

    }

    @Test
    public void saveBalanceSnaps() {
        List<TblBalanceSnaps> snaps = new ArrayList<>();
        for (long x=0; x < 10000; x++) {
            TblBalanceSnaps snap = new TblBalanceSnaps();
            snap.setUnitId(83l);
            snap.setAmount((long) 55.5);
            snap.setAccountId(123456789102345l);
            snap.setName("Test");
            snap.setAccount(x);
            snaps.add(snap);
        }
        balanceSnapsDao.saveBalanceSnaps(snaps);
    }

}