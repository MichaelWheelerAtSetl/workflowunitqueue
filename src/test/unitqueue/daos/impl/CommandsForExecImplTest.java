package unitqueue.daos.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import unitqueue.configuration.HibernateAdminConfiguration;
import unitqueue.daos.CommandsForExecDao;
import unitqueue.pojos.CommandIdPK;
import unitqueue.pojos.ViewCommandsForExec;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by michaelwheeler on 2017/02/28.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateAdminConfiguration.class})
public class CommandsForExecImplTest extends AbstractTransactionalJUnit4SpringContextTests
{

final static String sourceFile = "./sql/insertTestData.sql";
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private CommandsForExecDao commandsForExecDao;

    @Before
    public void setUp() {
        List<String> script = loadScript(sourceFile);
        for (String s : script) {
            sessionFactory.getCurrentSession().createSQLQuery(s).executeUpdate();
        }
    }

    private static List<String> loadScript(String filename) {
        File file = new File(filename);
        List<String>commands = new ArrayList<>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                commands.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return commands;
    }

    @Test
    public void testGetRecordsForProcessing() {
        List<ViewCommandsForExec> commandsToExec = commandsForExecDao.getCommandsToExecute();
        assertEquals(4, commandsToExec.size());
        for (ViewCommandsForExec command  : commandsToExec) {
            assertNull(command.getDependency());
        }
    }
    @Test
    public void testUpdateRecordAsProcessed() {
        //update command as executed where
        ViewCommandsForExec viewCommandsForExec = mock(ViewCommandsForExec.class  );
        CommandIdPK commandIdPK = new CommandIdPK();
        when(viewCommandsForExec.getCommandId()).thenReturn(commandIdPK);
        viewCommandsForExec.getCommandId().setAccountId(1l);
        viewCommandsForExec.getCommandId().setUnitId(90l);
        viewCommandsForExec.getCommandId().setStep(1);
        viewCommandsForExec.getCommandId().setSubStep(0);
        commandsForExecDao.updateCommandAsExecuted(viewCommandsForExec);

        List<ViewCommandsForExec> commandsToExec = commandsForExecDao.getCommandsToExecute();
        assertEquals(3, commandsToExec.size());

    }
    @Test
    public void testUpdateStepWithDependency() {

        ViewCommandsForExec viewCommandsForExec = mock(ViewCommandsForExec.class  );
        CommandIdPK commandIdPK = new CommandIdPK();
        when(viewCommandsForExec.getCommandId()).thenReturn(commandIdPK);
        viewCommandsForExec.getCommandId().setAccountId(1l);
        viewCommandsForExec.getCommandId().setUnitId(90l);
        viewCommandsForExec.getCommandId().setStep(2);
        viewCommandsForExec.getCommandId().setSubStep(0);
        commandsForExecDao.updateCommandAsExecuted(viewCommandsForExec);
        List<ViewCommandsForExec> commandsToExec = commandsForExecDao.getCommandsToExecute();
        assertEquals(4, commandsToExec.size());
    }

}



